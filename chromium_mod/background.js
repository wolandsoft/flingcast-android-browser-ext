var tabStats = [];

function getTabStats (id) {
	let tabStat = tabStats[id];
	if (tabStat === undefined || tabStat === null) {
		tabStat = resetTabStats(id);
	}
	return tabStat;
}

function resetTabStats (id) {
	let tabStat = {
		id: -1,
		completed: false,
		media_url: null,
		captured_url: null,
		fcid: null
	};
	tabStats[id] = tabStat;
	return tabStat;
}

chrome.tabs.onCreated.addListener((tab) => {
	resetTabStats(tab.id);
})

chrome.tabs.onRemoved.addListener((tabId, removeInfo) => {
	resetTabStats(tabId);
})

chrome.tabs.onUpdated.addListener((tabId, changeInfo, tab) => {
	if (changeInfo.status) {
		let ts = getTabStats(tabId);
		if (changeInfo.status === STATUS_LOADING && ts.completed) {
			// new page loading
			chrome.pageAction.hide(tabId);
			resetTabStats(tabId);
		} else if (changeInfo.status === STATUS_COMPLETE) {
			ts.completed = true;
		}
	}
});

chrome.runtime.onMessage.addListener((message, sender, resp) => {
	if (message.type){
		let type = message.type;
		let fcid = message.fcid;
		let tabId = sender.tab.id;
		let ts = getTabStats(tabId);
		if(type === EVENT_VIDEO_PLAYING) {
			console.log('EVENT_VIDEO_PLAYING', message.url)
			if (message.url !== '') {
				if (message.url.startsWith('blob:')) {
					ts.media_url = ts.captured_url;
				} else {
					ts.media_url = message.url;
				}
				if (ts.media_url !== null) {
					console.log('onUpdated-showing', tabId, message, sender);
					chrome.pageAction.show(tabId);
					chrome.pageAction.setIcon({
						tabId: tabId,
						path: {
							"19": "icons/app-19.png",
							"38": "icons/app-38.png"
						}
					});
					ts.fcid = fcid;
				}
			}
		} else if (type === EVENT_VIDEO_PAUSED) {
			console.log('EVENT_VIDEO_PAUSED', message.url)
			if (ts.fcid === fcid) {
				chrome.pageAction.hide(tabId);
				chrome.pageAction.setIcon({
					tabId: tabId,
					path: {
						"19": "icons/app-19-disabled.png",
						"38": "icons/app-38-disabled.png"
					}
				});
			}
		}
	}
});

chrome.pageAction.onClicked.addListener((tab) => {
	let ts = getTabStats(tab.id);
	chrome.tabs.sendMessage(tab.id, {
		type: EVENT_PA_BUTTON_CLICK,
		url: ts.media_url
	});
})

function logHEADER(requestDetails) {
	if (requestDetails.responseHeaders !== undefined && requestDetails.responseHeaders.length > 0) {
		var found = requestDetails.responseHeaders.find(function(header) {
			return header.name.toLowerCase() === CONTENT_TYPE;
		});
		if (found !== null) {
			var val = found.value.toLowerCase();
			if (SUPPORTED_MIME_TYPES.includes(val)) {
				console.log('logHEADER', requestDetails.url)
				let ts = getTabStats(requestDetails.tabId);
				ts.captured_url = requestDetails.url;				
			}
		}
	}
}

chrome.webRequest.onHeadersReceived.addListener(
  logHEADER,            
  {urls: ["*://*/*.m3u8", "*://*/*.m3u8?*"]},
  ["responseHeaders"]
)