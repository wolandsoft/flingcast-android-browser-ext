const EVENT_VIDEO_PLAYING = 'video-playing';
const EVENT_VIDEO_PAUSED = 'video-paused';
const EVENT_PA_BUTTON_CLICK = 'page-action-button-click';

const STATUS_LOADING = 'loading';
const STATUS_COMPLETE = 'complete';

const CONTENT_TYPE = 'content-type';
const SUPPORTED_MIME_TYPES = [
	'application/vnd.apple.mpegurl',
	'application/x-mpegurl'
];
