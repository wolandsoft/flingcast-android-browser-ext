String.prototype.trunc = 
	function(n){
		return this.substr(0,n-1)+(this.length>n?'…':'');
	};

function genid() {
	return '_' + Math.random().toString(36).substr(2, 9);
}

var activePlayer = null;
function scanPlayers () {
	var players = document.getElementsByTagName('video');
	for (var i = 0; i < players.length; i++) {
		var player = players[i];
		if (player.flingCastId === undefined) {
			player.flingCastId = genid();
			player.flingCastPlayListener = function(event) {
				let pl = event.srcElement;
				activePlayer = pl;
				browser.runtime.sendMessage({
					type: EVENT_VIDEO_PLAYING,
					fcid: pl.flingCastId,
					url: pl.currentSrc
				});
			};
			player.addEventListener("playing", player.flingCastPlayListener);
			player.flingCastPauseListener = function(event) {
				let pl = event.srcElement;
				browser.runtime.sendMessage({
					type: EVENT_VIDEO_PAUSED,
					fcid: pl.flingCastId,
					url: pl.currentSrc
				});
			};
			player.addEventListener("pause", player.flingCastPauseListener);
			if (!player.paused) {
				player.flingCastPlayListener({srcElement: player});
			}
		}
	}
}
var delayedScan = null;
function scanPlayersDelayed () {
	if (delayedScan) {
		clearTimeout(delayedScan);
	}
	delayedScan = setTimeout(function(){ 
		scanPlayers(); 
	}, 300);
}

browser.runtime.onMessage.addListener(message => {
	if (message.type) {
		if (message.type === EVENT_PA_BUTTON_CLICK) {
			if (activePlayer) {
				activePlayer.pause();
			}
			if (window.self === window.top) {
				setTimeout(() => {
					document.location = "intent://wolandsoft/#Intent;scheme=flingcast;package=com.wolandsoft.flingcast"
					+ ";S.browser_fallback_url=" + encodeURIComponent("market://details?id=com.wolandsoft.flingcast")
					+ ";S.media=" + encodeURIComponent(message.url)
					+ ";S.title=" + encodeURIComponent(document.title.trunc(64))
					+ ";end"
				}, 100);
			}
		}
	}
});

// Options for the observer (which mutations to observe)
const config = { 
	attributes: false,
	childList: true,
	subtree: true,
};
// Callback function to execute when mutations are observed
const callback = function(mutationsList, observer) {
	loopout:
	for(let mutation of mutationsList) {
		for (let target of mutation.addedNodes) {
			if(target.tagName && target.tagName.toLowerCase() === 'video') {
				scanPlayersDelayed();
				break loopout;
			}
		}
	}
};
// Create an observer instance linked to the callback function
const observer = new MutationObserver(callback);
// Start observing the target node for configured mutations
observer.observe(document, config);

scanPlayersDelayed ();