var tabStats = [];

function getTabStats (id) {
	let tabStat = tabStats[id];
	if (tabStat === undefined || tabStat === null) {
		tabStat = resetTabStats(id);
	}
	return tabStat;
}

function resetTabStats (id) {
	let tabStat = {
		id: -1,
		completed: false,
		media_url: null,
		captured_url: null,
		fcid: null
	};
	tabStats[id] = tabStat;
	return tabStat;
}

browser.tabs.onCreated.addListener((tab) => {
	resetTabStats(tab.id);
})

browser.tabs.onRemoved.addListener((tabId, removeInfo) => {
	resetTabStats(tabId);
})

browser.tabs.onUpdated.addListener((tabId, changeInfo, tab) => {
	if (changeInfo.status) {
		let ts = getTabStats(tabId);
		if (changeInfo.status === STATUS_LOADING && ts.completed) {
			// new page loading
			browser.pageAction.hide(tabId);
			resetTabStats(tabId);
		} else if (changeInfo.status === STATUS_COMPLETE) {
			ts.completed = true;
		}
	}
});

browser.runtime.onMessage.addListener((message, sender, resp) => {
	if (message.type){
		let type = message.type;
		let fcid = message.fcid;
		let tabId = sender.tab.id;
		let ts = getTabStats(tabId);
		if (type === EVENT_VIDEO_PLAYING) {
			if (message.url !== '') {
				if (message.url.startsWith('blob:')) {
					ts.media_url = ts.captured_url;
				} else {
					ts.media_url = message.url;
				}
				if (ts.media_url !== null) {
					browser.pageAction.show(tabId);
					ts.fcid = fcid;
				}
			}
		} else if (type === EVENT_VIDEO_PAUSED) {
			if (ts.fcid === fcid) {
				browser.pageAction.hide(tabId);
			}
		}
	}
});

browser.pageAction.onClicked.addListener((tab) => {
	let ts = getTabStats(tab.id);
	browser.tabs.sendMessage(tab.id, {
		type: EVENT_PA_BUTTON_CLICK,
		url: ts.media_url
	});
})

function logHEADER(requestDetails) {
	if (requestDetails.responseHeaders !== undefined && requestDetails.responseHeaders.length > 0) {
		var found = requestDetails.responseHeaders.find(function(header) {
			return header.name.toLowerCase() === CONTENT_TYPE;
		});
		if (found !== null) {
			var val = found.value.toLowerCase();
			if (SUPPORTED_MIME_TYPES.includes(val)) {
				let ts = getTabStats(requestDetails.tabId);
				ts.captured_url = requestDetails.url;				
			}
		}
	}
}

browser.webRequest.onHeadersReceived.addListener(
  logHEADER,            
  {urls: ["*://*/*.m3u8", "*://*/*.m3u8?*"]},
  ["responseHeaders"]
)